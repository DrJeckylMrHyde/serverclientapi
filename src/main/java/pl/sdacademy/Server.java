package pl.sdacademy;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Date;

public class Server {
    public static void main(String[] args) throws IOException {

        ServerSocket listener = new ServerSocket(666);
        System.out.println("Date server is running");

        while(true) {
            Socket socket = listener.accept();

//        to odczytywalo date
//        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
//        out.println(new Date().toString());

//        liczym sume podanych liczb przez klienta
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String response = bufferedReader.readLine();
            System.out.println(response);

            String[] numbers = response.split(" ");

            int result = Arrays.stream(numbers)
                    .mapToInt(Integer::parseInt)
                    .sum();
            System.out.println(result);

            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(result);
            socket.close();
        }
    }
}
